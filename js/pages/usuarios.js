function toggleSignOut(event){
    event.preventDefault();
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
      }).catch(function(error) {
        // An error happened.
      });
}

function initApp() {
    // Listening for auth state changes.
    // [START authstatelistener]
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
      }
      else{
        window.location.href = "login.html";
      } 
    });
    document.getElementById('sign-out').addEventListener('click', function(event){
      toggleSignOut(event);
    }, false);
  }
  window.onload = function() {
    initApp();
    getUsers();
    document.getElementById('btnUsers').addEventListener('click', function(event){
        updateUsers(event);
    }, false);

  };

  function getUsers() {
    var database = firebase.database();
    let arrPromises = [];
    var arrJson = [];
    return database.ref('/Holo/usuarios').once('value').then(function(snapshot) {
      snapshot.forEach(usuariosSnapshot => {
        arrPromises.push(database.ref('Holo/usuarios/' + usuariosSnapshot.key).once('value').then(function(childSnapshot) {
          var key = usuariosSnapshot.key;
          var nombre = childSnapshot.child("nombre").val();
          var estrellas = parseInt(childSnapshot.child("estrellas").val());
          var puntosCredito = parseInt(childSnapshot.child("puntos_credito").val());
          var puntosDeuda = parseInt(childSnapshot.child("puntos_deuda").val());
          var puntosHolo = parseInt(childSnapshot.child("puntos_holo").val());
          var puntosOxxo = parseInt(childSnapshot.child("puntos_oxxo").val());
          var servicios = parseInt(childSnapshot.child("servicios").val());
          arrJson.push([
            key,
            nombre,
            estrellas,
            puntosCredito,
            puntosDeuda,
            puntosHolo,
            puntosOxxo,
            servicios
          ]);
        }));
      });
      Promise.all(arrPromises).then((result) => {
        console.log(arrJson);
        $('#users').DataTable({
          data: arrJson,
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<a class='cyan-text' id='btnEditar'> <span>Editar</span></a>"
          }]
        });

        var table = $('#users').DataTable();
        $('#users tbody').on('click', 'a', function() {
          var data = table.row($(this).parents('tr')).data();
          current_key = data[0];
          $('#nombre').val(data[1]);
          $('#estrellas').val(data[2]);
          $('#puntosCredito').val(data[3]);
          $('#puntosDeuda').val(data[4]);
          $('#puntosHolo').val(data[5]);
          $('#puntosOxxo').val(data[6]);
          $('#servicios').val(data[7]);
        });
      });
    });
  }

  var current_key;
  function updateUsers (event){
    var nombrel = $('#nombre').val() ? $('#nombre').val() : "EmptyorNull";
    var estrellasl = $('#estrellas').val()? parseInt($('#estrellas').val()) : "EmptyorNull";
    var puntosCreditol = $('#puntosCredito').val()? parseInt($('#puntosCredito').val()) : "EmptyorNull";
    var puntosDeudal = $('#puntosDeuda').val()? parseInt($('#puntosDeuda').val()) : "EmptyorNull";
    var puntosHolol = $('#puntosHolo').val()? parseInt($('#puntosHolo').val()) : "EmptyorNull";
    var puntosOxxol = $('#puntosOxxo').val()? parseInt($('#puntosOxxo').val()) : "EmptyorNull";
    var serviciosl = $('#servicios').val()? parseInt($('#servicios').val()) : "EmptyorNull";
    if(nombrel === "" || estrellasl === "" || puntosCreditol === "" || puntosDeudal === "" || puntosHolol === "" || puntosOxxol === "" || serviciosl === "" || nombrel === "EmptyorNull" || estrellasl === "EmptyorNull" || puntosCreditol === "EmptyorNull" || puntosDeudal === "EmptyorNull" || puntosHolol === "EmptyorNull" || puntosOxxol === "EmptyorNull" || serviciosl === "EmptyorNull"){
      return;
    }
    event.preventDefault();
    var database = firebase.database();

    database.ref('Holo/usuarios/' + current_key).update({
      nombre: nombrel,
      estrellas: estrellasl,
      puntos_credito: puntosCreditol,
      puntos_deuda: puntosDeudal,
      puntos_holo: puntosHolol,
      puntos_oxxo: puntosOxxol,
      servicios: serviciosl
    }, function(error){
      if(error){
        console.log("error", error);
        Materialize.toast({html: 'Error' + error});
      } else{
        console.log("success");
        current_key = "";
        location.reload();
      }
    });
  };

  
  