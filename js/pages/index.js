function toggleSignOut(event) {
	event.preventDefault();
	firebase.auth().signOut().then(function() {
	}).catch(function(error) {
	});
}

function initApp() {
	firebase.auth().onAuthStateChanged(function(user) {
		if (user) {} else {
			window.location.href = "login.html";
		}
	});
	document.getElementById('sign-out').addEventListener('click', function(event) {
		toggleSignOut(event);
	}, false);
}
window.onload = function() {
	initApp();
	getRoutes('calificada');
};
$(document).ready(function() {
	$('.dropdown-trigger').dropdown();
});
var map;
var route;
var arrJson = [];
var arrdata = [];
var table;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: 21.123495,
			lng: -101.6771277
		},
		zoom: 13
	});
	infoWindow = new google.maps.InfoWindow;
}
function getRoutes(estatus) {
	var database = firebase.database();
	let arrPromises = [];
	arrJson = [];
	arrdata = [];
	var arrVectores = [];
	var vectores;
	return database.ref('/Holo/Rutas').orderByChild('estatus').equalTo(estatus).once('value').then(function(snapshot) {
		if (snapshot.exists()) {
			snapshot.forEach(rutasSnapshot => {
				arrPromises.push(database.ref('Holo/Rutas/' + rutasSnapshot.key).once('value').then(function(childSnapshot) {
					var key = rutasSnapshot.key;
					var origen = childSnapshot.child("dir_origen").val();
					var destino = childSnapshot.child("dir_destino").val();
					var horaInicio = childSnapshot.child("hora_inicio").val();
					var hora_Salida = childSnapshot.child("hora_salida").val();
					vectores = childSnapshot.child("vectores").val();
					arrJson.push([
						key
					]);
					arrVectores.push(vectores);
					arrdata.push([
						key,
						origen,
						destino,
						horaInicio,
						hora_Salida
					]);
				}));
			});
			Promise.all(arrPromises).then((result) => {
				if (table) {
					table.rows().invalidate().destroy();
					route.setMap(null);
				}
				table = $('#estatusTable').DataTable({
					data: arrJson,
					columnDefs: [{
						targets: -1,
						data: null,
						defaultContent: "<a class='cyan-text' id='btnShow'> <span>Información</span></a>"
					}],
					retrieve: true,
					searching: false,
					lengthChange: false
				});
				table = $('#estatusTable').DataTable();
				$('#estatusTable tbody').on('click', 'a', function() {
					var data = table.row($(this).parents('tr')).data();
					var current_key = data[0];
					for (let i = 0; i < arrdata.length; i++) {
						if (current_key == arrdata[i][0]) {
							$("#key").text(arrdata[i][0]);
							$("#origen").text(arrdata[i][1]);
							$("#destino").text(arrdata[i][2]);
							$("#horaInicio").text(arrdata[i][3]);
							$("#horaSalida").text(arrdata[i][4]);
							$('#modalData').openModal();
						}
					}
				});
				for (let i = 0; i < arrVectores.length; i++) {
					var color;
					var r = Math.floor(Math.random() * 255);
					var g = Math.floor(Math.random() * 255);
					var b = Math.floor(Math.random() * 255);
					color = "rgb(" + r + " ," + g + "," + b + ")";
					drawRoutes(arrVectores[i], color);
				}
			});
		} else {
			Materialize.toast("No hay rutas con estatus: " + estatus, 3000);
		}
	});
}

function drawRoutes(vectores, color) {
	var poly = [];
	for (let c = 0; c < vectores.length; c++) {
		var coords = new google.maps.LatLng(vectores[c].latitud, vectores[c].longitud)
		poly.push(coords);
	}
	route = new google.maps.Polyline({
		path: poly,
		geodesic: true,
		strokeColor: color,
		strokeOpacity: 1,
		strokeWeight: 3,
		map: map
	});
}
$(document).ready(function() {
	$('#dropCreada').click(function() {
		getRoutes("creada");
	});
	$('#dropProxima').click(function() {
		getRoutes("proxima");
	});
	$('#dropCancelado').click(function() {
		getRoutes("cancelado");
	});
	$('#dropcalificada').click(function() {
		getRoutes("calificada");
	});
	$('#dropConPendienteEn').click(function() {
		getRoutes("conductor_pendiente_por_encontrar");
	});
	$('#dropPasPendienteCon').click(function() {
		getRoutes("pasajero_pendiente_por_confirmar");
	});
	$('#dropConPendienteCon').click(function() {
		getRoutes("conductor_pendiente_por_confirmar");
	});
});