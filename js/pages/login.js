
    /**
     * Handles the sign in button press.
     */
    function toggleSignIn(event) {
      if (firebase.auth().currentUser) {
        // [START signout]
        firebase.auth().signOut();
        // [END signout]
      } else {
        var email = document.getElementById('email').value ? document.getElementById('email').value: "EmptyorNull";
        var password = document.getElementById('password').value ? document.getElementById('password').value: "EmptyorNull";
        if (email.length < 4 || email === "EmptyorNull") {
          return;
        }
        if (password.length < 4 || password === "EmptyorNull") {
          return;
        }
        // Sign in with email and pass.
        // [START authwithemail]
        event.preventDefault();
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // [START_EXCLUDE]
          document.getElementById('email').value = "";
          document.getElementById('password').value = "";
          if (errorCode === 'auth/wrong-password') {
            Materialize.toast("Contraseña incorrecta",3000);
            console.log('Contraseña incorrecta');
            return;
          } else if(errorCode === "auth/user-not-found"){
            console.log('usuario no encontrado');
            Materialize.toast("Usuario no encontrado", 3000);

            return;
          } else {
            console.log(errorMessage);
            return;
          }
        });
        // [END authwithemail]
      }
    }
    

    function initApp() {
      // Listening for auth state changes.
      // [START authstatelistener]
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          //Redireccionar a index
          window.location.href = "index.html";
        }
        else{
          
        } 
      });
      document.getElementById('quickstart-sign-in').addEventListener('click', function(event){
        toggleSignIn(event);
      }, false);
    }
    window.onload = function() {
      initApp();
    };
