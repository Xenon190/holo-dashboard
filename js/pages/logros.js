function toggleSignOut(event){
    event.preventDefault();
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
      }).catch(function(error) {
        // An error happened.
      });
}

function initApp() {
    // Listening for auth state changes.
    // [START authstatelistener]
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
      }
      else{
        window.location.href = "login.html";
      } 
    });
    document.getElementById('sign-out').addEventListener('click', function(event){
      toggleSignOut(event);
    }, false);
  }
  window.onload = function() {
    initApp();
    getLogros();
    document.getElementById('btnLogros').addEventListener('click', function(event){
      updateLogros(event);
    }, false);

  };

  function getLogros() {
    var database = firebase.database();
    let arrPromises = [];
    var arrJson = [];
    return database.ref('/Holo/logros').once('value').then(function(snapshot) {
      snapshot.forEach(logrosSnapshot => {
        arrPromises.push(database.ref('Holo/logros/' + logrosSnapshot.key).once('value').then(function(childSnapshot) {
          var key = logrosSnapshot.key;
          var nombre = childSnapshot.child("nombre").val();
          var fecha = childSnapshot.child("fecha_logro").val();
          var contenido = childSnapshot.child("texto").val();
          var activo = parseInt(childSnapshot.child("activo").val());
          arrJson.push([
            key,
            nombre,
            fecha,
            contenido,
            activo
          ]);
        }));
      });
      Promise.all(arrPromises).then((result) => {
        console.log(arrJson);
        $('#logros').DataTable({
          data: arrJson,
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<a class='cyan-text' id='btnEditar'> <span>Editar</span></a>"
          }]
        });

        var table = $('#logros').DataTable();
        $('#logros tbody').on('click', 'a', function() {
          var data = table.row($(this).parents('tr')).data();
          current_key = data[0];
          $('#nombre').val(data[1]);
          $('#activo').val(data[4]);
          $('#fecha').val(data[2]);
          $('#texto').val(data[3]);
        });
      });
    });
  }

  var current_key;
  function updateLogros (event){
    var nombrel = $('#nombre').val() ? $('#nombre').val() : "EmptyorNull";
    var activol = $('#activo').val()? parseInt($('#activo').val()) : "EmptyorNull";
    var fechal = $('#fecha').val()? $('#fecha').val() : "EmptyorNull";
    var textol = $('#texto').val()? $('#texto').val() : "EmptyorNull";
    if(nombrel === "" || activol === "" || fechal === "" || textol === "" || nombrel === "EmptyorNull" || activol === "EmptyorNull" || fechal === "EmptyorNull" || textol === "EmptyorNull"){
      return;
    }
    event.preventDefault();
    var database = firebase.database();

    database.ref('Holo/logros/' + current_key).update({
      activo: activol,
      fecha_logro: fechal,
      nombre: nombrel,
      texto: textol
    }, function(error){
      if(error){
        console.log("error", error);
        Materialize.toast({html: 'Error' + error});
      } else{
        console.log("success");
        current_key = "";
        location.reload();
      }
    });
  };

  
  