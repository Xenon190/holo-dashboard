function toggleSignOut(event){
    event.preventDefault();
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
      }).catch(function(error) {
        // An error happened.
      });
}

function initApp() {
    // Listening for auth state changes.
    // [START authstatelistener]
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
      }
      else{
        window.location.href = "login.html";
      } 
    });
    document.getElementById('sign-out').addEventListener('click', function(event){
      toggleSignOut(event);
    }, false);
  }

  window.onload = function() {
    initApp();
    document.getElementById('btnCreateLogros').addEventListener('click', function(event){
      createLogros(event);
    }, false);
  };

  function createLogros(event){
    var nombrel = $('#nombre').val() ? $('#nombre').val() : "EmptyorNull";
    var activol = $('#activo').val()? parseInt($('#activo').val()) : "EmptyorNull";
    var fechal = $('#fecha').val()? $('#fecha').val() : "EmptyorNull";
    var textol = $('#texto').val()? $('#texto').val() : "EmptyorNull";
    if(nombrel === "" || activol === "" || fechal === "" || textol === "" || nombrel === "EmptyorNull" || activol === "EmptyorNull" || fechal === "EmptyorNull" || textol === "EmptyorNull"){
      return;
    }
    event.preventDefault();
    var database = firebase.database();

    database.ref('Holo/logros/').push().set({
      activo: activol,
      fecha_logro: fechal,
      nombre: nombrel,
      texto: textol
    }, function(error){
      if(error){
        console.log("error", error);
        Materialize.toast({html: 'Error' + error});
      } else{
        console.log("success");
        location.reload();
      }
    });
  }

  
  